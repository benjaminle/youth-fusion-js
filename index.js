const express = require('express');
const fs = require('fs');
const bodyparser = require('body-parser');
const app = express();
const nodemailer = require('nodemailer');
const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'bridgetstone123456@gmail.com',
        pass: 'SpaceToday'
    }
});

// var cookieParser = require('cookie-parser');
//
// app.use(cookieParser);
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());
app.use(express.static('public'));
app.use(express.static('views/components'));

app.get('/',(req, res) => {
    res.sendFile(__dirname + '/views/mainPage.html');
});

app.get('/our_team', (req, res) => {
    res.sendFile(__dirname + '/views/team_members.html');
})

app.get('/partner_schools', (req, res) => {
    res.sendFile(__dirname + '/views/fs_schools.html');
});

app.get('/non_profit_encyclopedia', (req, res) => {
    res.sendFile(__dirname + '/views/fo_encyclopedia.html');
})

app.get('/partner_organizations', (req, res) => {
    res.sendFile(__dirname + '/views/fo_orgs.html');
});

app.get('/collaborate', (req, res) => {
    res.sendFile(__dirname + '/views/collaborate.html');
});

app.get('/check_news', (req,res) => {
    res.sendFile(__dirname + '/views/fv_news.html');
});

app.get('/news_template', (req, res) => {
    res.sendFile(__dirname + '/views/news_template.html');
});

app.get('/training', (req, res) => {
    var list = parseCookies(req);

    if (list.key === undefined || list.key === NaN) {
        res.redirect("/login");
    } else {
        res.sendFile(__dirname + '/views/fv_training.html');
    }
});

app.get('/register_as_volunteer', (req, res) => {
    res.sendFile(__dirname + '/views/fv_volunteer.html');
});

app.get('/login', (req,res) => {
   res.sendFile(__dirname + '/views/login/login.html');
});

app.get('/getCode', (req, res) => {
    //Send user code
    var code = "";
    if (userCodes.length >= 1000) {
        let rand = Integer.valueOf(Math.random() * 1000);
        res.send(userCodes[rand]);
    }
    for (var i = 0; i < 24; i++) {
        code += String.fromCharCode(Math.floor(Math.random() * 26 + 97));
    }
    userCodes.push(code);
    res.send(code);
});

app.get('/abcdefghijklmnopqrstuvwxyz', (req, res) => {
    saveToFile(userCodes, "usercodes");
    saveToFile(userEmails, "useremails");
    saveVolunteersLoginFile(volunteerslogin, "volunteerslogin");
    saveScoreFile(volunteersscore, "volunteersscore");
    //Save file
    res.send(true);
});

app.post('/checkLogin', (req, res) => {
    var code = req.body.code;

    if (userCodes.includes(code)) {
        res.cookie('key', code, {maxAge: 90000, httpOnly: true})
        res.redirect("/training");
    } else {
        res.redirect("/login");
    }
    //Check user login
});

app.post('/postEmail', (req, res) => {
    var email = req.body.email;


    if (email != "" && !userEmails.includes(email)) {
        userEmails.push(email);
    }
    res.redirect("/");

});

app.get('/signup_email', (req, res) => {
    res.sendFile(__dirname + '/views/fv_email.html');
});

app.post('/send_volunteer_email', (req,res) => {
    var genEmail = req.body.email;
    var genCode = "";
    var contains = false;
    var index = 0;

    for (var i = 0; i < volunteerslogin.length; i++) {
        if (genEmail == undefined || volunteerslogin[i].email == genEmail) {
            contains = true;
            ;
            index = i;
        }
    }
    if (!contains) {
        for (var i = 0; i < 15; i++) {
            genCode += String.fromCharCode(Math.floor(Math.random() * 26 + 97));
        }
        var object = {
            email: genEmail,
            code: genCode,
        }
        volunteerslogin.push(object);
        sendEmail(genEmail, genCode);
        res.redirect('/signup_code');
    } else if (genEmail != "") {
        sendEmail(genEmail, volunteerslogin[index].code);
        res.redirect("/signup_code");
    } else {
        res.status(400).send("Your request is missing details.");
    }
});

app.get('/signup_code', (req,res) => {
    var list = parseCookies(req);
    var jl = list.jl;
    if (jl == "supercalifragilisticexpialidocious") {
        res.redirect("/prelim-questions");
    } else {
        res.sendFile(__dirname + '/views/logincode.html');
    }
})

app.post('/send_login_code', (req,res) => {
    var code = req.body.login_code;
    var list = parseCookies(req);
    var access;
    var email = list.email;

    for (let i = 0; i < volunteerslogin.length; i++) {
        if (volunteerslogin[i].email == email) {
            if (volunteerslogin[i].code == code) {
                res.cookie('jl', 'supercalifragilisticexpialidocious',
                    {maxAge:1000*60*60, httpOnly: true });
                access = true;
                break;
            }
        }
    }
    if (access) {
        res.redirect("/prelim-questions");
    } else {
        res.send("failure");
    }
});

app.get('/prelim-questions', (req, res) => {

    var list = parseCookies(req);
    var access = list.jl;

    if (access == "supercalifragilisticexpialidocious") {
        res.sendFile(__dirname + '/views/prelim_questions.html');
    } else {
        res.redirect('/register_as_volunteer');
    }

})

app.post('/send-score', (req, res) => {
    var email = req.body.email;
    var score = req.body.correct;
    var contains = false;
    if (email == undefined) {
        console.log("undefined");
        return;
    }
    for (var i = 0; i < volunteersscore.length; i++) {
        if (volunteersscore[i].email == email && volunteersscore[i].hasTaken) {
            contains = true;

            break;
        }
    }

    if (!contains) {
        var object = {
            email: email,
            score: score,
            hasTaken: 1
        };
        volunteersscore.push(object);
        console.log("here");
        res.redirect('/results');
    }
})

app.get('/results', (req, res) => {
    var list = parseCookies(req);
    var email = list.email;

    if (email == undefined || email =="") {
        res.redirect('/register_as_volunteer');
    } else {
        var hasTaken = false;
        for (var i = 0; i < volunteersscore.length; i++) {
            if (volunteersscore[i].email == email && volunteersscore[i].hasTaken) {
                hasTaken = true;
                res.sendFile(__dirname + "/views/results.html");
            }
        }
        if (hasTaken) {
            res.sendFile(__dirname + '/views/results.html');
        } else {
            res.redirect('/register_as_volunteer');
        }
    }


});

//Download files

app.get('/usercodes', (req, res) => {
    res.sendFile(__dirname + '/usercodes');
})

//End download files

function parseCookies (request) {
    var list = {},
        rc = request.headers.cookie;

    rc && rc.split(';').forEach(function( cookie ) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });

    return list;
}

function readFromFile (datapath) {

    var array = [];

    let input = fs.createReadStream(datapath);

    var rl = require('readline').createInterface({
        input: input,
        terminal: false,
    });

    rl.on('line', function(line) {
        array.push(line);
    });

    return array;
}

function saveToFile (array, datapath) {
    var line = "";
    for (var i = 0; i < array.length; i++) {
        line += array[i] + "\n";
    }
    fs.writeFileSync(datapath, line);
}


var userCodes = [];

userCodes = readFromFile("usercodes");

var userEmails = [];

userEmails = readFromFile("useremails");

var volunteersscore = [] ;

volunteersscore = readScoreFile("volunteersscore");

/* Post: read information form volunteers
        info and their scores
 */
function readScoreFile(datapath) {
    var array = [];
    let input = fs.createReadStream(datapath);

    var rl = require('readline').createInterface({
        input: input,
        terminal: false
    });

    rl.on('line', function(line) {
        var lineSplit = line.split(":");
        var object = {
            email: lineSplit[0],
            score: lineSplit[1],
            hasTaken: lineSplit[2]
        };
        array.push(object);
    });
    return array;
}

/* Post: save score file */
function saveScoreFile(array, datapath) {
    var line = "";
    for (var i = 0; i < array.length; i++) {
        line += array[i].email + ":" + array[i].score + ":" + array[i].hasTaken + "\n";
    }
    fs.writeFileSync(datapath, line);
}

app.get('/getScoreFile', (req, res) => {
    res.sendFile(__dirname + '/volunteersscore');
});

var volunteerslogin = new Array();
volunteerslogin = readVolunteersLoginFile("volunteerslogin");

function readVolunteersLoginFile(datapath) {
    var arr = [];

    let input = fs.createReadStream(datapath);

    var rl = require('readline').createInterface({
        input: input,
        terminal: false
    });

    rl.on('line', function(line) {
        var lineSplit = line.split(":");
        var object = {
            email: lineSplit[0],
            code: lineSplit[1]
        };
        arr.push(object);
    });

    return arr;
}

function saveVolunteersLoginFile(array, datapath) {
    var line = "";
    for (var i = 0; i < array.length; i++) {
        line += array[i].email + ":" + array[i].code + "\n";
    }
    fs.writeFileSync(datapath, line);
}

app.get('/sendEmail', (req, res) => {
    sendEmail("benjaminle2002@gmail.com", "hey");
    res.send(true);
});

function sendEmail(email, code) {
    var mailOptions = {
        from: 'bridgetstone123456@gmail.com',
        to: email,
        subject: 'Sending Emails to Who? Space',
        text: code
    };

    setInterval(send,60*60*1000);
    send();
    function send() {
        transporter.sendMail(mailOptions, function(error, info){
        });
    }
}
const port = process.env.PORT || 3000;

app.listen(port,() => console.log(`listening on port ${port}`));

